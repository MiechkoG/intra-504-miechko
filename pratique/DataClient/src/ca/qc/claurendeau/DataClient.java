package ca.qc.claurendeau;

import bean.LigneDeCourant;
import ca.qc.claurendeau.exceptions.DataException;
import ca.qc.claurendeau.exceptions.DatabaseException;
import ca.qc.claurendeau.exceptions.NetworkException;

public class DataClient {
	private static final int MAX_VOLTAGE = 800;
	DataServer dataserver;
	int voltage;

	public DataClient(DataServer dataServer) {
		this.dataserver = dataServer;
	}

	public int processData(int entryID) {
		initTransaction(entryID);

		LigneDeCourant ligne = getLigneData();

		calculateOutput(ligne);

		sendVoltageToServer(ligne);

		finalizeTransaction();

		return voltage;
	}

	public void initTransaction(int entryID) {
		try {
			dataserver.initiateNewTransaction(entryID);
		} catch (DatabaseException e) {
			System.out.println("Trying again new- " + e.getMessage());
			initTransaction(entryID);
		}
	}

	public LigneDeCourant getLigneData() {
		String csvData = null;
		while (csvData == null) {
			try {
				csvData = dataserver.getCSVData();
			} catch (DataException e) {
				System.out.println("Trying again get- " + e.getMessage());
				getLigneData();
			}
		}
		return parseCSV(csvData);
	}

	public LigneDeCourant parseCSV(String csvData) {
		String[] parsedCSV = csvData.split(",");
		LigneDeCourant ligne = new LigneDeCourant();
		ligne.setId(Integer.valueOf(parsedCSV[0]));
		ligne.setInputLeft(Integer.parseInt(parsedCSV[1]));
		ligne.setInputRight(Integer.valueOf(parsedCSV[2]));
		ligne.setCoefficient(Integer.valueOf(parsedCSV[3]));
		return ligne;
	}

	public int calculateOutput(LigneDeCourant ligne) {
		int output = ligne.getInputLeft() + ligne.getInputRight();
		ligne.setOutput(output);
		return output;
	}

	public void sendVoltageToServer(LigneDeCourant ligne) {
		if (canBeSent(ligne)) {
			try {
				dataserver.sendVoltage(ligne.getOutput(), ligne.getCoefficient());
			} catch (NetworkException e) {
				System.out.println("Trying again send- " + e.getMessage());
				sendVoltageToServer(ligne);
			}
			voltage = ligne.getOutput() * ligne.getCoefficient();
		} else {
			voltage = -1;
		}
	}

	public boolean canBeSent(LigneDeCourant ligne) {
		System.out.println("Checking if OK to send voltage...");
		if (ligne.getVoltage() < MAX_VOLTAGE) {
			System.out.println("OK! \n");
			return true;
		} else {
			System.out.println("Not OK! \n");
			return false;
		}

	}

	public void finalizeTransaction() {
		try {
			dataserver.finalizeTransaction();
		} catch (DatabaseException e) {
			System.out.println("Trying again close- " + e.getMessage());
			finalizeTransaction();
		}
	}

}
