package bean;

public class LigneDeCourant {
	private int id;
	private int inputLeft;
	private int inputRight;
	private int coefficient;
	private int output;

	public LigneDeCourant() {
		super();
	}

	public LigneDeCourant(int id, int inputLeft, int inputRight, int coefficient) {
		super();
		this.id = id;
		this.inputLeft = inputLeft;
		this.inputRight = inputRight;
		this.coefficient = coefficient;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getInputLeft() {
		return inputLeft;
	}

	public void setInputLeft(int inputLeft) {
		this.inputLeft = inputLeft;
	}

	public int getInputRight() {
		return inputRight;
	}

	public void setInputRight(int inputRight) {
		this.inputRight = inputRight;
	}

	public int getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(int coefficient) {
		this.coefficient = coefficient;
	}

	public int getOutput() {
		return output;
	}

	public void setOutput(int output) {
		this.output = output;
	}

	public int getVoltage() {
		return (inputLeft + inputRight) * coefficient;
	}

	@Override
	public String toString() {
		return "LigneDeCourant [id=" + id + ", inputLeft=" + inputLeft + ", inputRight=" + inputRight + ", coefficient="
				+ coefficient + "]";
	}

}
