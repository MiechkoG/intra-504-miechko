package ca.qc.claurendeau;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.xml.sax.DocumentHandler;

import bean.LigneDeCourant;
import ca.qc.claurendeau.exceptions.DataException;
import ca.qc.claurendeau.exceptions.NetworkException;

@RunWith(MockitoJUnitRunner.class)
public class DataClientTest {
	DataServer dataserver;
	DataClient dataClient;
	LigneDeCourant ligne01;
	LigneDeCourant ligne02;
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		dataserver = Mockito.mock(DataServer.class);
		dataClient = new DataClient(dataserver);
		ligne01 = new LigneDeCourant(1, 100, 100, 4);
		ligne02 = new LigneDeCourant(1, 10, 10, 10);
	}
	@Test
	public void testCanBeSent() {
		assertFalse(dataClient.canBeSent(ligne01));
		assertTrue(dataClient.canBeSent(ligne02));
	}

	@Test
	public void testCalculateOutput(){
		assertEquals(dataClient.calculateOutput(ligne01), 200);
	}
	
	@Test 
	public void testParseCSV(){
		assertEquals(dataClient.parseCSV("1,10,10,10").toString(), ligne02.toString());
	}
	
	@Test
	public void testGetLigneData() throws DataException{
		Mockito.when(dataserver.getCSVData()).thenReturn("1,10,10,10");
		
		assertEquals(dataClient.getLigneData().toString(), ligne02.toString());
	}

	@Test
	public void testProcessData() throws DataException{
		Mockito.when(dataserver.getCSVData()).thenReturn("1,10,10,10");
		
		assertEquals(dataClient.processData(1), 200);
	}
	
}
