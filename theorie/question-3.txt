﻿Question 3 (10 pts)
-------------------

a. (2.5 pts) Quelle configuration minimale devriez-vous avoir dans ${HOME}/.gitconfig ? Expliquez pourquoi.

b. (2.5 pts) Expliquez comment fonctionne l'opération 'git commit'. Donnez un exemple.

c. (5 pts) Expliquez comment fonctionne 'git rebase'. Illustrez votre explication par un exemple.


a) la location de l'origin, du repository git, ce qui me permmetra de push les commits a la bonne place, de plus, pour la claireté dans les commits il est important de setter le username de l'utilisateur.

b) git commit prend les modifications apportées au fichiers qui ont été ajouté au staging et envoie ces modifications dans le répertoire du projet git local, il est important d'ajouter des commentaires pour laisser des traces de modifications. par exemple, si je crée un nouveau ficher 'bean' et que je l'ajoute au staging avec la commande 'git stage bean', je dois par la suite 'git commit', ce qui sauveguarde ces changements, avec un commentaire, dans la liste de différences entre les données locales et celle sur git.

c) lorsqu'on rebase, on crée un nouveau commit qui sera 'up-to-date'. git va prendre le commit commun le plus récent (2) et implémenter les changements a l'intérieur, ce qui crée une nouvelle branche 'master' (5) qui contient toutes les modifications des nodes 3 et 4 dans ce cas -ci.

	      /-------------3 ------------- 5 (new master)
1 --------- 2  -------------4 /